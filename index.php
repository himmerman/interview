  <!-- header begin -->
  <? include 'includes/header.php';?>
	<!-- header close -->
    
	<!-- slider -->
	<div id="slider">
    	<div class="callbacks_container">
        	<ul class="rslides pic_slider">
             	<li>
               		<img src="img/slider-home/pic2.jpg" alt="">
                	<div class="slider-info">
                	<!-- <h1>Welcome to Yellowstone Inn</h1> -->
                	</div>
                </li>  
                <li>
               		<img src="img/slider-home/pic3.jpg" alt="">
                	<div class="slider-info">
                	<!-- <h1>It's our way to make you feel more than home</h1> -->
                	</div>
                </li>
                <li>
                  <img src="img/slider-home/pic4.jpg" alt="">
                  <div class="slider-info">
                  <!-- <h1>Stay in our cozy cabin-styled lodging</h1> -->
                  </div>
                </li> 
                 <li>
                  <img src="img/slider-home/pic1.jpg" alt="">
                  <div class="slider-info">
                  <!-- <h1>It's our way to make you feel more than home</h1> -->
                  </div>
                </li>   
                                                     
        	</ul>
    	</div>
    </div>
	<!-- slider close -->
    
    <div class="clearfix"></div>
    
	<!-- search begin -->
   	<div id="booking">
    	<div class="container">
        	<div class="row">
        	<span class="span2">Book Now:</span>
        	<form class="form-inline">
            <input type=hidden name=respage id=respage value="https://v2.reservationkey.com/3809/reserve">
            	<div class="span2">
                	<input type="text" name="checkin" id="checkin" value="Check In Date">
                </div>
                <div class="span2">
            		<input type="text" name="checkout" id="checkout" value="Check Out Date">
            </div>
            	<!-- <div class="span2">
            		<select>
                   		<option>Select Room</option>
                        <option>Deluxe Room </option>
                        <option>Elegant Room</option>
                        <option>luxury Room</option>
                    </select>
                </div>             -->	
                <div class="span2">
                <select name="numguests" id="numguests">
                   		<option>Number of Guests</option>
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>

                  </select>
        </div>
                <div class="span2">
                <a onclick="window.location=document.getElementById('respage').value +'/s|'+ document.getElementById('checkin').value.replace(/\//g,'.')+'|'+ document.getElementById('checkout').value.replace(/\//g,'.')+'|'+ document.getElementById('numguests').value" class="btn btn-pimary btn-submit">Check Availability</a>
                </div>
        	</form>
      	</div>
      </div>
	</div>
	<!-- search close -->

	<!-- content begin -->
    <div id="content">
    	<div class="container">
        	
		
           <div class="row">
            <div class="span12">
              <h1 style="text-align:center;">Unique West Yellowstone Inn cabins and Lodging at its best!</h1>
              <br/>

            </div>
             
           </div>
           <div class="row">

            <div class="span2"></div>
             <div class="span8">
              <p>Our <a href="rooms-rates.php">West Yellowstone Cabins</a> and rooms are just a few minutes’ drive away from the the National Park entrance, and conveniently located on highway 20, which also leads through town. We offer <a href="rooms-rates.php">lodging with</a> individuality and character that combines the rugged feel of Yellowstone with modern day conveniences. Franchised hotels and motels in West Yellowstone simply don’t offer the <a href="rooms-rates.php">unique accommodations</a> and ambience available at our family-owned Inn. We are only minutes away from all of the stores, outfitters and fly shops in West Yellowstone. Enjoy the Yellowstone wildlife, and the beautiful scenic views and natural wonders as you hike or drive through the Park. Anglers appreciate that their hosts know the difference between a lake trout, cutthroat, and brook trout.</p>
              <p>Yellowstone Inn is your home away from home.  <a href="rooms-rates.php">Our unique Yellowstone Cabins and rooms</a> sleep between two and five guests, many with their own kitchens and fireplaces. centrally located between Madison, Firehole, and Yellowstone rivers, our <a href="rooms-rates.php">cabins and lodge-style motel rooms</a> in West Yellowstone are perfect for fishermen, hikers, and sightseers. Most fishermen want little more than to wake up, eat, fish, and sleep, and the Inn puts them only minutes away from Hebgen Lake. Door racks are provided to properly care for your wet waders and boots while you get the sleep you need to bag the next day’s catch.</p>


             </div>
             <div class="span2">
               <div id="TA_cdsratingsonlynarrow671" class="TA_cdsratingsonlynarrow">
                  <ul id="zeKTyBkQBoO" class="TA_links eAjoBAn7qY">
                  <li id="Iwtey38q" class="r4GkTmOSLqLD">
                  <a target="_blank" href="http://www.tripadvisor.com/"><img src="http://www.tripadvisor.com/img/cdsi/img2/branding/tripadvisor_logo_transp_340x80-18034-2.png" alt="TripAdvisor"/></a>
                  </li>
                  </ul>
                  </div>
                  <script src="http://www.jscache.com/wejs?wtype=cdsratingsonlynarrow&amp;uniq=671&amp;locationId=567120&amp;lang=en_US&amp;border=true&amp;display_version=2"></script>
                  <br>
                  <div class="btn-book-container">
                      <a href="https://v2.reservationkey.com/3809/reserve" class="btn btn-primary btn-submit">Check Availability</a>                  
                    </div>
             </div>
           </div>

             <!-- <hr> -->
            <!--  <div class="row">
              <div class="span3 feature">
                <i class="icon-map-marker icon-3x"></i><br>
                <h4>Convenient location</h4>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
              </div>
                
              <div class="span3 feature">
                <i class="icon-calendar icon-3x"></i><br>
                <h4>Reserve Online</h4>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
              </div>
              
              <div class="span3 feature">
                <i class="icon-home icon-3x"></i><br>
                <h4>Cozy Log Cabins</h4>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
              </div>
              
              <div class="span3 feature">
                <i class="icon-thumbs-up icon-3x"></i><br>
                <h4>Your Best Option</h4>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
              </div>
            </div>        -->     
             <div class="row gallery">
             <br>
             <hr>
             	<div class="text-center">
                    <h2>Hotel Gallery</h2>
                    Find your favorite room, feel more than home<br><br>
   			   </div>
                 
                 <div class="span2">
           	     	<a class="preview" href="img/gallery/1.JPG" rel="prettyPhoto" title="Your Title">
           	     	<img data-original="img/gallery/1.JPG" src="img/gallery/1.JPG" class="img-polaroid" alt="">
                    </a>
                 </div>
                 
                 <div class="span2">
           	     	<a class="preview" href="img/gallery/8.JPG" rel="prettyPhoto" title="Your Title">
           	     	<img data-original="img/gallery/8.JPG" src="img/gallery/8.JPG" class="img-polaroid" alt="">
                    </a>
                 </div>
                 
                 <div class="span2">
           	     	<a class="preview" href="img/gallery/3.JPG" rel="prettyPhoto" title="Your Title">
           	     	<img data-original="img/gallery/3.JPG" src="img/gallery/3.JPG" class="img-polaroid" alt="">
                    </a>
                 </div>
                 
                 <div class="span2">
           	     	<a class="preview" href="img/gallery/4.JPG" rel="prettyPhoto" title="Your Title">
           	     	<img data-original="img/gallery/4.JPG" src="img/gallery/4.JPG" class="img-polaroid" alt="">
                    </a>
                 </div>
                 
                 <div class="span2">
           	     	<a class="preview" href="img/gallery/9.JPG" rel="prettyPhoto" title="Your Title">
           	     	<img data-original="img/gallery/9.JPG" src="img/gallery/9.JPG" class="img-polaroid" alt="">
                    </a>
                 </div>
                 
                 <div class="span2">
           	     	<a class="preview" href="img/gallery/6.JPG" rel="prettyPhoto" title="Your Title">
           	     	<img data-original="img/gallery/6.JPG" src="img/gallery/6.JPG" class="img-polaroid" alt="">
                    </a>
                 </div>
                 
             </div>
            
            
            

             
             
            </div>
        
        </div>
	<!-- content close -->
    
    <!-- footer begin -->
  <? include 'includes/footer.php';?>

	
	<!-- footer close -->
   
</body>
</html>
