  <!-- header begin -->
  <? include 'includes/header.php';?>
  <!-- header close -->
    
    <!-- subheader begin -->
<!-- 	<div id="subheader">
    	<div class="container">
    	  <div class="row">
          	<div class="span12">
            	<h1>Contact Us</h1>
                <span>Have a question? We'd love to hear from you!</span>
            </div>
          </div>
    	</div>
    </div> -->
	<!-- subheader close -->  
        
   <div id="map-container">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d11351.036008640842!2d-111.10972655680818!3d44.663275272300254!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x5351b9eae9c847c1%3A0xe7963c37af24a7b9!2sYellowstone+Inn!5e0!3m2!1sen!2sus!4v1420347274039" frameborder="0" style="border:0"></iframe>
     <!--    <iframe frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.co.id/maps?q=itc+depok&amp;hl=id&amp;sll=-6.248411,106.833801&amp;sspn=0.371998,0.529404&amp;ie=UTF8&amp;view=map&amp;f=d&amp;daddr=Jl.+Margonda+Raya+Kav+56,+Depok+16431&amp;geocode=CXBfEqpsWd8iFV5wnv8dev5dBiFfQlEfkZZ3Xw&amp;ll=-6.393762,106.82329&amp;spn=0.006295,0.006335&amp;output=embed"></iframe> -->
   </div>
    
	<!-- content begin -->
    <div id="content">
    	<div class="container">
        	<div class="row">
            	<div class="span8">
                	<h3>Get in touch with us now!</h3>
Feel free to contact us to get more information or <a href="https://v2.reservationkey.com/3809/reserve">Reserve online now</a><br/><br/>
                	<div class="contact_form_holder">
              <form id="contact" class="row" name="form1" method="post" action="mailer.php">
              
       			<div class="span4">
                <label>Name</label>
           		<input type="text" class="full" name="name" id="name"/>
           		</div>
                
                <div class="span4">
                <label>Email <span class="req">*</span></label>
           		<input type="text" class="full" name="email" id="email"/>
                <div id="error_email" class="error">Please check your email</div> 
				</div>
				
                <div class="span8">
                <label>Message <span class="req">*</span></label>
                <textarea cols="10" rows="10" name="message" id="message" class="full"></textarea>
                <div id="error_message" class="error">Please check your message</div>
                <div id="mail_success" class="success"> Thank you. Your message has been sent.</div>
<div id="mail_failed" class="error">Error, email not sent</div>

<p id="btnsubmit"><input type="submit" id="send" value="Send" class="btn btn-large"/></p>               
                </div>
                
                    
              </form>
              <?php
                if (isset($_GET['m'])) {
                  echo "<p>Thank you for message! We will be in contact with you shortly.</p>";
                } else if (isset($_GET['e'])) {
                  echo "<p>Please fill out the form correctly and resubmit</p>";
                }
              ?>
            </div>
            
                </div> 
                    
              <div id="sidebar" class="span4">
              		

              
              		 <!-- widget category --><!-- widget tags --><!-- widget text -->
       	  <div class="widget widget-text">
                	<h3>Our Address</h3>
                      <address>
                                601 US 20 West Yellowstone, MT
                        <span><strong>Phone:</strong>1.406.646.7633</span>
                        <!-- <span><strong>Email:</strong><a href="mailto:contact@example.com">contact@example.com</a></span> -->
                        <span><strong>Web:</strong><a href="http://yellowstoneinn.net">www.yellowstoneinn.net</a></span>
                        <span><strong>E-Mail:</strong>yellowstoneinn1@gmail.com</span>
                        <span><strong>Mail:</strong>Postal address PO Box 1139 West Yellowstone, MT  59758</span>
                        
                       </address>
       	  </div>
              </div>
        	</div>
</div>
        </div>
    </div>
	<!-- content close -->
    
    <!-- footer begin -->
  <? include 'includes/footer.php';?>

  
  <!-- footer close -->
   
</body>
</html>

