  <? include 'includes/header.php';?>
  <!-- header close -->

    

    <!-- subheader begin -->

	<div id="subheader">

    	<div class="container">

    	  <div class="row">

          	<div class="span12">

            	<h1>Amenities</h1>

                <span>Cabins and Lodge-styled accommodations</span>

            </div>

          </div>

    	</div>

    </div>

	<!-- subheader close -->  

        

   

    

	<!-- content begin -->

    <div id="content">

      <div class="container">

        <!-- left column -->

        <div class="row">

        <div class="span12">
            <h1> Family-Owned Cabin and Lodge Style Accommodation in West Yellowstone</h1>
            <h4>Pets are no longer allowed to stay at the Yellowstone Inn.</h4>
        </div>

          <div class="span8">

            

            <br/>

            <p><a href="rooms-rates.php">Lodging in West Yellowstone</a> should be built in the rugged spirit of the park. It wouldn’t do to take on bear country in a glitzy hotel. You need to be surrounded by timber and stone.  With log furniture, rock fireplaces, and arched loft ceilings, you have the authentic rustic retreat you travelled across country to experience.  Though we’re not fancy, we’re just right, with all the modern amenities and comforts you’ll need during your stay with us. <a href="https://v2.reservationkey.com/3809/reserve">Click here to plan your stay</a>.</p>

            <p><a href="rooms-rates.php">Our Yellowstone cabins</a> feature knotty pine interiors, Pine timbers, with log furnishings, microwaves and mini fridges, and free Wireless internet. Yellowstone Inn guests enjoy our grassy commons area, with picnic tables and outdoor grill for you to enjoy an afternoon or evening barbecue.  We offer <a href="rooms-rates.php">several cabins for groups</a>, as well as connecting rooms. Come see why our guests keep coming back year after year, and leaving great reviews. In West Yellowstone, <a href="rooms-rates.php">Cabins and rooms</a> fill up fast! <a href="https://v2.reservationkey.com/3809/reserve">Check availability</a> or <a href="https://v2.reservationkey.com/3809/reserve">book online now</a>.</p>

          </div>
      <!-- right column -->

          <div class="span4">

           <h4>Features:</h4>

            <ul>

              <li>Knotty Pine Interiors</li>

              <li>Unique Cabin Furniture</li>
              <li>Free Wifi</li>

              <li>Cable TV</li>

              <li>Coffee Makers</li>

              <li>Microwaves</li>

              <li>Small Refrigerators</li>

              <li>Air Conditioners in Summer</li>

              <li>Individually Controlled Electric Heat</li>

              
            </ul>
            <br>
            <div class="btn-book-container">
                    <a href="https://v2.reservationkey.com/3809/reserve" class="btn btn-primary btn-submit">Check Availability</a>                  
                  </div>

            <!-- <img data-original = "img/amenities/1-bryce-canyon-hotels.jpg" src = "img/pic-blank-1.gif" alt = "">

            <img data-original = "img/amenities/3-bryce-canyon-hotel.jpg" src = "img/pic-blank-1.gif" alt = ""> -->

                   



          </div>
          </div>
          <br>
         
          <div class="row">
            <div id="gallery" class="gallery">
                  
              <!-- <h4>Gallery item --> 
              <div class="span3 item int isotope-item">
                <a class="preview" href="img/amenities/amenities1.jpg" rel="prettyPhoto" title="Queen Beds">
                  <img data-original = "img/amenities/amenities1.jpg" src = "img/amenities/amenities1.jpg" alt="">
                </a>
                <h4>Queen Beds</h4>
                <span></span>
              </div>

              <div class="span3 item ext isotope-item">
                <a class="preview" href="img/amenities/amenities2.jpg" rel="prettyPhoto" title="Unit #6">
                  <img data-original = "img/amenities/amenities2.jpg" src = "img/amenities/amenities2.jpg" alt = "">
                </a>
                <h4>Unit #6</h4>
                <span></span>
              </div>
              <div class="span3 item ext isotope-item">
                <a class="preview" href="img/amenities/amenities3.jpg" rel="prettyPhoto" title="Double Queen">
                  <img data-original = "img/amenities/amenities3.jpg" src = "img/amenities/amenities3.jpg" alt = "">
                </a>
                <h4>Double Queen</h4>
                <span></span>
              </div>
              <div class="span3 item int isotope-item">
                <a class="preview" href="img/amenities/amenities4.jpg" rel="prettyPhoto" title="Spacious Living Rooms">
                  <img data-original = "img/amenities/amenities4.jpg" src = "img/amenities/amenities4.jpg" alt = "">
                </a>
                <h4>Spacious Living Rooms</h4>
                <span></span>
              </div>
              <div class="span3 item int isotope-item">
                <a class="preview" href="img/amenities/amenities5.jpg" rel="prettyPhoto" title="Kitchen Units">
                  <img data-original = "img/amenities/amenities5.jpg" src = "img/amenities/amenities5.jpg" alt="">
                </a>
                <h4>Kitchen Units</h4>
                <span></span>
              </div>

              <div class="span3 item int isotope-item">
                <a class="preview" href="img/amenities/amenities6.jpg" rel="prettyPhoto" title="Bathrooms">
                  <img data-original = "img/amenities/amenities6.jpg" src = "img/amenities/amenities6.jpg" alt="">
                </a>
                <h4>Bathrooms</h4>
                <span></span>
              </div>

              <div class="span3 item int isotope-item">
                <a class="preview" href="img/amenities/amenities7.jpg" rel="prettyPhoto" title="Lofts for the kids">
                  <img data-original = "img/amenities/amenities7.jpg" src = "img/amenities/amenities7.jpg" alt="">
                </a>
                <h4>Lofts for the kids</h4>
                <span></span>
              </div>

              <div class="span3 item int isotope-item">
                <a class="preview" href="img/amenities/amenities8.jpg" rel="prettyPhoto" title="Living Rooms/TV's">
                  <img data-original = "img/amenities/amenities8.jpg" src = "img/amenities/amenities8.jpg" alt="">
                </a>
                <h4>Living Rooms/TV's</h4>
                <span></span>
              </div>

            </div>
          
        
        


          
          </div>

          

        </div>

      </div>

      

  <!-- content close -->

  





<!-- footer begin -->
    <!-- footer begin -->
  <? include 'includes/footer.php';?>

  
  <!-- footer close -->

  <!-- footer close -->

   

</body>

</html>



