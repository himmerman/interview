<? include 'includes/header.php';?>
  <!-- header close -->
    
    <!-- subheader begin -->
	<div id="subheader">
    	<div class="container">
    	  <div class="row">
          	<div class="span12">
            	<h1>Attractions Near Us</h1>
                <span>Explore the best of Yellowstone</span>
            </div>
          </div>
    	</div>
    </div>
	<!-- subheader close -->  
   
    
	<!-- content begin -->
    <div id="content">

      <div class="container">
      <div class="row">
        
          
      
        <div class="span12">
          <h1>Your Yellowstone National Park Cabins and Vacation Lodging</h1>
        
          <br/>
          <p>There’s no place quite like Yellowstone. There’s a reason Yellowstone is the world’s first national park. Come and see world-renowned geysers like Old Faithful, Hot Springs, towering waterfalls, set in rugged, beautiful wilderness. Yellowstone National Park offers glimpses of swans, elk, grizzlies, mountain lions, and don’t forget the Bison herds roaming in plain sight. (They really are as big as a compact car!) The Lower Falls is a breathtaking waterfall set in what is called the Grand Canyon of Yellowstone. And of course, we can’t forget Old Faithful, the geyser famous for it’s regular, spectacular eruptions reaching heights of over 130 feet. Our cabins fill up fast. <a href="https://v2.reservationkey.com/3809/reserve">Plan your vacation with us today</a>.</p>
          <p>Our <a href="rooms-rates.php">lodge-style Motel rooms and Cabins</a> fit the rugged, rustic feel that people the world over have come to expect from a former frontier, park entrance town like West Yellowstone. </p>
          <p>Visit Grizzly Overlook for choice osprey and bald eagle viewing opportunities. Pelican Creek Trail is a great place for catching a glimpse of Grizzlies, and Hebgen Lake is a fly fisherman’s dreamscape. The Grizzly and Wolf Discovery Center gets you up close and personal with one of Yellowstone’s iconic predators.  The Playmill Theater and IMAX put you in contact with Yellowstone’s more cultural side. </p>
          <br/>
        </div>

      <!-- right column -->
          <!-- <div class="span4">
           <h4>Things to see/do:</h4>
            <ul>
              <li>Old Faithful</li>
              <li>Bison</li>
              <li>Fishing</li>
              <li>Fountain Geyser</li>
              <li>Mammoth Hot Springs</li>
              <li>Yellowstone Lake</li>
              <li>Elk</li>
              
            </ul>
          </div> -->
        </div>
        <div class="row">
        
          <div class="span3">
            <div class="btn-book-container">
                    <a href="https://v2.reservationkey.com/3809/reserve" class="btn btn-primary btn-submit">Check Availability</a>                  
                  </div>
          </div>
        </div>
        <br>
          <div class="row">
              <div class="span12">
                  <ul id="filters">
                      <li><a href="#" data-filter="*" class="selected">show all</a></li>
                      <li><a href="#" data-filter=".wildlife">Wildlife</a></li>
                      <li><a href="#" data-filter=".scene">Scenery</a></li>
                      <li><a href="#" data-filter=".activity">Activities</a></li>
                      <!-- <li><a href="#" data-filter=".promotion">promotion</a></li> -->
                    </ul>

                </div>
            </div>
          <div class="row">             
              <div id="gallery" class="gallery">
                  
                    <!-- <h4>Gallery item --> 
                  <div class="span3 item scene isotope-item">
                    <a class="preview" href="img/gallery/fountaingeyser_small.jpg" rel="prettyPhoto" title="Your Title">
                      <img src="img/gallery/fountaingeyser_small.jpg" data-original="img/gallery/fountaingeyser_small.jpg" alt="">
                    </a>
                  <h4>Fountain Geyser</h4>
                    <span></span>
                    </div>
                    <!-- close <h4>Gallery item -->
                    
                    <!-- <h4>Gallery item --> 
                  <div class="span3 item wildlife isotope-item">
                    <a class="preview" href="img/gallery/bison_baby_small.jpg" rel="prettyPhoto" title="Your Title">
                      <img src="img/gallery/bison_baby_small.jpg" data-original="img/gallery/bison_baby_small.jpg" alt="">
                    </a>
                    <h4>Bison</h4>
                    <span></span>
                  </div>
                    <!-- close <h4>Gallery item -->
                    
                    <!-- <h4>Gallery item --> 
                  <div class="span3 item promotion wildlife isotope-item">
                    <a class="preview" href="img/gallery/bear_small.jpg" rel="prettyPhoto" title="Your Title">
                      <img src="img/gallery/bear_small.jpg" data-original="img/gallery/bear_small.jpg" alt="">
                    </a>
                  <h4>Grizzly Bear</h4>
                    <span></span>
                    </div>
                    <!-- close <h4>Gallery item -->
                    
                    <!-- <h4>Gallery item --> 
                  <div class="span3 item activity isotope-item">
                    <a class="preview" href="img/gallery/fishingsmall.jpg" rel="prettyPhoto" title="Your Title">
                      <img src="img/gallery/fishingsmall.jpg" data-original="img/gallery/fishingsmall.jpg" alt="">
                    </a>
                  <h4>Fishing</h4>
                    <span></span>
                    </div>
                    <!-- close <h4>Gallery item -->
                    
                    <!-- <h4>Gallery item --> 
                  <div class="span3 item scene isotope-item">
                    <a class="preview" href="img/gallery/yellowstonelake.jpg" rel="prettyPhoto" title="Your Title">
                      <img src="img/gallery/oldfaithful-800.jpg" data-original="img/gallery/oldfaithful-800.jpg" alt="">
                    </a>
                  <h4>Old Faithful</h4>
                    <span></span>
                    </div>
                    <!-- close <h4>Gallery item -->
                    
                    <!-- <h4>Gallery item --> 
                  <div class="span3 item scene isotope-item">
                    <a class="preview" href="img/gallery/mammothhotspring.jpg" rel="prettyPhoto" title="Your Title">
                      <img src="img/gallery/yellowstonefalls-800.jpg" data-original="img/gallery/yellowstonefalls-800.jpg" alt="">
                    </a>
                  <h4>Lower Falls</h4>
                    <span></span>
                    </div>
                    <!-- close <h4>Gallery item -->
                    
                    <!-- <h4>Gallery item --> 
                  <div class="span3 item wildlife isotope-item">
                    <a class="preview" href="img/gallery/elk.jpg" rel="prettyPhoto" title="Your Title">
                      <img src="img/gallery/elk.jpg" data-original="img/gallery/elk.jpg" alt="">
                    </a>
                  <h4>Elk</h4>
                    <span></span>
                    </div>
                    <!-- close <h4>Gallery item -->
                    
                    <!-- <h4>Gallery item --> 
                  <div class="span3 item scene isotope-item">
                    <a class="preview" href="img/gallery/oldfaithful.jpg" rel="prettyPhoto" title="Your Title">
                      <img src="img/gallery/grandprismaticspring-800.jpg" data-original="img/gallery/grandprismaticspring-800.jpg" alt="">
                    </a>
                  <h4>Grand Prismatic Spring</h4>
                    <span></span>
                    </div>
                    <!-- close <h4>Gallery item -->
                    
                </div>
            </div>
        </div>
        

      </div>
  <!-- content close -->
    
    <!-- footer begin -->
  <? include 'includes/footer.php';?>

  
  <!-- footer close -->
   
</body>
</html>

