 <!-- header begin -->
  <? include 'includes/header.php';?>
  <!-- header close -->
    
    <!-- subheader begin -->
	<div id="subheader">
    	<div class="container">
    	  <div class="row">
          	<div class="span12">
            	<h1>Cabins and Lodge-style Motel rooms in West Yellowstone</h1>

                <span></span>
            </div>
          </div>
    	</div>
    </div>
	<!-- subheader close -->  
   
    
	<!-- content begin -->
    <div id="content">
      <div class="container">
        <!-- <h1>Cabins and Lodge-style Motel rooms in West Yellowstone</h1> -->
         <h4>Pets are no longer allowed to stay at the Yellowstone Inn.</h4>
         <br>
      <div class="row room-list">
            
              <!-- room -->
        <div class="room span4">
          <div class="btn-book-container">
            <a href="https://v2.reservationkey.com/3809/reserve" class="btn-book">Book Now</a>
          </div>
          <a href="https://v2.reservationkey.com/3809/reserve"><img src="img/cabins/cabin1.jpg" data-original="img/cabins/cabin1.jpg" class="img-polaroid" alt=""></a>
         
          <h4>Room 1</h4>
          <div class="description">
            TwTwo Queen beds full bath, fully equipped kitchen w/ Coffee Pot, Microwave, Refrigerator and living room w/ fireplace and fold out futon. Wi-Fi, satellite T.V. w/ HBO. Includes window air conditioning unit and baseboard heaters. Picnic tables and chairs are available along with BBQ grills
          </div>
         
         
        </div>
                <!-- close room -->
                
                  <!-- room -->
                 <div class="room span4">
                    <div class="btn-book-container">
                    <a href="https://v2.reservationkey.com/3809/reserve" class="btn-book">Book Now</a>
                    </div>
                   <a href="https://v2.reservationkey.com/3809/reserve"><img src="img/cabins/cabin2.jpg" data-original="img/cabins/cabin2.jpg" class="img-polaroid" alt=""></a>
                    <h4>Room 2</h4>
                    <div class="description">
                    Two Queens with Full Bathroom, Coffee Pots, Microwave, Refrigerator, Wi-Fi, satellite T.V. w/ HBO. This room also has a connecting door to room 3 for larger parties. Includes window air conditioning unit and baseboard heaters. Picnic tables and chairs are available along with BBQ grills
                    <br>
                  </div>
                     
                     
                </div>
                <!-- close room -->


              <!-- room -->
                 <div class="room span4">
                    <div class="btn-book-container">
                    <a href="https://v2.reservationkey.com/3809/reserve" class="btn-book">Book Now</a>
                    </div>
                  <a href="https://v2.reservationkey.com/3809/reserve"><img src="img/cabins/cabin3.jpg" data-original="img/cabins/cabin3.jpg" class="img-polaroid" alt=""></a>
                    <h4>Room 3</h4>
                    <div class="description">
                    Two Queens w/ Full Bathroom, Microwave, Refrigerator, Coffee Pot, Wi-Fi, satellite T.V. w/ HBO. This room also has a connecting door to room 2 for larger parties. Includes window air conditioning unit and baseboard heaters. Picnic tables and chairs are available along with BBQ grills
                  </div>
                     
                     
                </div>
                <!-- close room -->
                </div>
                <div class="row room-list">
                <!-- room -->
        <div class="room span4">
          <div class="btn-book-container">
            <a href="https://v2.reservationkey.com/3809/reserve" class="btn-book">Book Now</a>
          </div>
          <a href="https://v2.reservationkey.com/3809/reserve"><img src="img/cabins/cabin4.jpg" data-original="img/cabins/cabin4.jpg" class="img-polaroid" alt=""></a>
          <h4>Room 4</h4>
          <div class="description">
           Two Queens w/ fireplace, full Bathroom, living room, recliner & couch, refrigerator, coffee pot, microwave, Wi-Fi, satellite T.V. w/ HBO. Includes window air conditioning unit and baseboard heaters. Picnic tables and chairs are available along with BBQ grills
          </div>
         
         
        </div>
                <!-- close room -->
          <!-- room -->
        <div class="room span4">
          <div class="btn-book-container">
            <a href="https://v2.reservationkey.com/3809/reserve" class="btn-book">Book Now</a>
          </div>
          <a href="https://v2.reservationkey.com/3809/reserve"><img src="img/cabins/cabin5.jpg" data-original="img/cabins/cabin5.jpg" class="img-polaroid" alt=""></a>
          <h4>Cabin 5</h4>
          <div class="description">
            One Queen bed, fully equipped kitchen w/ Coffee Pot, and Microwave. Full bathroom w/ shower only. Wi-Fi, satellite T.V. w/ HBO. Includes window air conditioning unit and baseboard heaters. Picnic tables and chairs are available along with BBQ grills
          </div>
         
         
        </div>
                <!-- close room -->

                <!-- room -->
        <div class="room span4">
          <div class="btn-book-container">
            <a href="https://v2.reservationkey.com/3809/reserve" class="btn-book">Book Now</a>
          </div>
         <a href="https://v2.reservationkey.com/3809/reserve"><img src="img/cabins/cabin6.jpg" data-original="img/cabins/cabin6.jpg" class="img-polaroid" alt=""></a>
          <h4>Cabin 6</h4>
          <div class="description">
            Cabin w/ two double beds up stairs, folding out futon down stairs. Full bathroom, fully equipped kitchen with coffee pot, refrigerator and microwave. Satellite T.V. w/ HBO and WIFI. This room is a two story with steep turning stairs please know this room is not ideal for small children or guests that are limited in movement. Includes window air conditioning unit and baseboard heaters. Picnic tables and chairs are available along with BBQ grills
          </div>
         
         
        </div>
                <!-- close room -->
                </div>
                <div class="row room-list">
                <!-- room -->
        <div class="room span4">
          <div class="btn-book-container">
            <a href="https://v2.reservationkey.com/3809/reserve" class="btn-book">Book Now</a>
          </div>
          <a href="https://v2.reservationkey.com/3809/reserve"><img src="img/cabins/cabin7.jpg" data-original="img/cabins/cabin7.jpg" class="img-polaroid" alt=""></a>
          <h4>Cabin 7</h4>
          <div class="description">
            One Queen bed, fully equipped kitchen w/ refrigerator, microwave, and a coffee pot. Full bathroom w/ shower only. Satellite T.V. w/ HBO, and WIFI. Includes window air conditioning unit and baseboard heaters. This room also has a connecting door to room 8 for larger parties. Picnic tables and chairs are available along with BBQ grills
          </div>
         
         
        </div>
                <!-- close room -->

                <!-- room -->
        <div class="room span4">
          <div class="btn-book-container">
            <a href="https://v2.reservationkey.com/3809/reserve" class="btn-book">Book Now</a>
          </div>
         <a href="https://v2.reservationkey.com/3809/reserve"><img src="img/cabins/cabin8.jpg" data-original="img/cabins/cabin8.jpg" class="img-polaroid" alt=""></a>
          <h4>Cabin 8</h4>
          <div class="description">
            One Queen bed, full bathroom w/ shower only. Coffee Pot, Microwave and, Refrigerator. Satellite T.V. w/ HBO, and WIFI. Includes window air conditioning unit and baseboard heaters. This room also has a connecting door to room 7 for larger parties. Picnic tables and chairs are available along with BBQ grills
          </div>
         
         
        </div>
                <!-- close room -->
                 <!-- room -->
        <div class="room span4">
          <div class="btn-book-container">
            <a href="https://v2.reservationkey.com/3809/reserve" class="btn-book">Book Now</a>
          </div>
         <a href="https://v2.reservationkey.com/3809/reserve"><img src="img/cabins/cabin9.jpg" data-original="img/cabins/cabin9.jpg" class="img-polaroid" alt=""></a>
          <h4>Cabin 9</h4>
          <div class="description">
            One Queen bed, full bathroom w/ shower only. Fireplace, Coffee Pot, Microwave, and Refrigerator. Satellite T.V. w/ HBO, and WIFI. Includes window air conditioning unit and baseboard heaters. Picnic tables and chairs are available along with BBQ grills
          </div>
         
         
        </div>
                <!-- close room -->
                </div>
                <div class="row room-list">
                 <!-- room -->
        <div class="room span4">
          <div class="btn-book-container">
            <a href="https://v2.reservationkey.com/3809/reserve" class="btn-book">Book Now</a>
          </div>
         <a href="https://v2.reservationkey.com/3809/reserve"><img src="img/cabins/cabin10.jpg" data-original="img/cabins/cabin10.jpg" class="img-polaroid" alt=""></a>
          <h4>Cabin 10</h4>
          <div class="description">
            Two double beds up stairs and a fold out futon down stairs w/ a fireplace, full bathroom, fully equipped kitchen w/ Coffee Pot, Microwave, and Refrigerator. Satellite T.V. w/ HBO, and WIFI. Please know this cabin is a two story with a very steep ladder, this cabin is not ideal for small children or guests with limited movement. Includes window air conditioning unit and baseboard heaters. Picnic tables and chairs are available along with BBQ grills
          </div>
         
         
        </div>
                <!-- close room -->
              </div>
           </div>
          </div>


    </div>
  <!-- content close -->
    
    <!-- footer begin -->
  <? include 'includes/footer.php';?>

  
  <!-- footer close -->
   
</body>
</html>

