<!DOCTYPE html>
<html lang="en">
<head>
    <? 
    $env = "development";
    $root = array("development" => "/yellowstone/", "production" => "/");
    $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    // var_dump($_SERVER['HTTP_HOST']);
    $path = $_SERVER['REQUEST_URI'];

    // var_dump($actual_link);
    // var_dump($_SERVER['REQUEST_URI']);
    $index = 'index.php';
    $amenities = "amenities.php";
    $attractions = "attractions.php";
    $rooms = "rooms-rates.php";
    $contact = "contact.php";
    
    $title = "Yellowstone Inn Cabins and Motel | West Yellowstone Lodging";

    $description = "A few minutes drive from Yellowstone National Park Entrance, just off of the Hwy, our cabins & lodge style motel rooms will make your stay a memorable one.";


    if(strpos($path, $amenities)){
      
      $title = "West Yellowstone National Park Accommodations and Lodging";
      $description = "Our unique knotty pine cabins, with pine log furniture offer microwaves, mini fridges, kitchens & stoves. Enjoy our outdoor picnic tables & grills.";
    } else if (strpos($path, $attractions)) {
      $title = "Motel Cabins Near Yellowstone National Park, Old Faithful";

      $description = "Minutes away from Yellowstone Park and scenic wildlife views, Henry’s fork river, Hebgen lake, Grizzly and Wolf Discovery Center, Playmill Theater, IMAX";
    } else if (strpos($path, $rooms)) {
      $title = "Lodge style motel and cabins in West Yellowstone Montana";

      $description = "Unique rooms and cabins will serve to relax you after a day of adventure in West Yellowstone Montana. Kitchens, fireplaces and connecting units available.";
    } else if (strpos($path, $contact)) {
      $title = "Yellowstone Inn West Yellowstone Montana Accommodations";

      $description = "Come see why so many guests return year after year. We look forward to serving you. Send Yellowstone Inn a message, call us directly, or reserve online.";
    } else if(strpos($path, $index) || $path == $root[$env]){
      $title = "Yellowstone Inn Cabins and Motel | West Yellowstone Lodging";

      $description = "A few minutes drive from Yellowstone National Park Entrance, just off of the Hwy, our cabins & lodge style motel rooms will make your stay a memorable one.";
    }

    ?>
    <meta charset="utf-8">
    <title><?= $title?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="<?= $description?>">
    <!-- <meta name="author" content=""> -->
    
    <!-- LOAD CSS FILES -->
    <link href="css/main.css" rel="stylesheet" type="text/css">
    
    <!-- LOAD JS FILES -->
   	<script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.isotope.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
     <script src="js/easing.js"></script>
    <script src="js/jquery.lazyload.js"></script>
    <script src="js/jquery.ui.totop.js"></script>
	<script src="js/selectnav.js"></script>
    <script src="js/ender.js"></script>
    <script src="js/custom.js"></script>
    <script src="js/responsiveslides.min.js"></script>
    <script src="js/datepickr.js"></script>
    <script>
		jQuery(function() {
		new datepickr('checkin');
		});
		jQuery(function() {
		new datepickr('checkout');
		});
	</script>
</head>

<body>
	<header>
        	<div id="logo">
            	<div class="inner">
                <div class="span7">
   	    		      <a href="index.php"><img src="img/logo2.png" alt=""></a>
                </div>

            	</div>
            </div>
            
                        
            <!-- mainmenu begin -->
            <div id="mainmenu-container">           
				<ul id="mainmenu">
                  <li><a class="<?= (strpos($path, '/index') || $path == $root[$env]) ? 'active' : '' ?>" href="index.php">Home</a></li>
                  <li><a class="<?= strpos($path, '/amenities') ? 'active' : '' ?>" href="amenities.php">Amenities</a></li>
                  <li><a class="<?= strpos($path, '/attractions') ? 'active' : '' ?>" href="attractions.php">Attractions</a></li>
                  <li><a class="<?= strpos($path, '/rooms-rates') ? 'active' : '' ?>" href="rooms-rates.php">Rooms &amp; Rates</a></li>
                  <!-- <li><a href="#"></a></li> -->
                  <li><a class="<?= strpos($path, '/contact') ? 'active' : '' ?>" href="contact.php">Contact</a></li>
              	</ul>
    		</div>

    </header>