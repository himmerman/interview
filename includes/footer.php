<footer>
  		<div class="container">
   	  		<div class="row">
                <div class="span3">
                	<img src="img/footerlogo.png" alt="">
                </div> 
            	<div class="span3">
                	<h3>Our Inn</h3>
                    Our lodge-style Motel rooms and Cabins fit the rugged, rustic feel that people the world over have come to expect from a former frontier, park entrance town like West Yellowstone. 
                </div>                                
 				
               <div class="span3">
                	<h3>Our Address</h3>
                    	<address>
                                601 US 20 West Yellowstone, MT
                        <span><strong>Phone:</strong>1.406.646.7633</span>
                        <!-- <span><strong>Email:</strong><a href="mailto:contact@example.com">contact@example.com</a></span> -->
                        <span><strong>Web:</strong><a href="http://yellowstoneinn.net">www.yellowstoneinn.net</a></span>
                        <span><strong>Mail:</strong>Postal address PO Box 1139 West Yellowstone, MT  59758</span>
                        
                       </address>
                       
                       <div class="social-icons">
                        	<!-- <a href="#"><img src="img/social-icons/rss.png" alt=""/></a> -->
                        	<!-- <a href="#"><img src="img/social-icons/facebook.png" alt=""/></a> -->
                        	<!-- <a href="#"><img src="img/social-icons/twitter.png" alt=""/></a>
                        	<a href="#"><img src="img/social-icons/gplus.png" alt=""/></a>
                        	<a href="#"><img src="img/social-icons/youtube.png" alt=""/></a>
                        	<a href="#"><img src="img/social-icons/vimeo.png" alt=""/></a> -->
                        </div>
                </div>
                <div class="span3">
                  <h3>Stay with us</h3>
                  <div class="btn-book-container">
                    <a href="https://v2.reservationkey.com/3809/reserve" class="btn btn-primary btn-submit">Check Availability</a>                  
                  </div>
                </div>
            </div>
        </div>
        
        <div class="subfooter">
        	<div class="container">
                <div class="row">
                	<div class="span6">
                    	&copy; Copyiright <?= date("Y")?> - Yellowstone Inn. Created By <a rel="nofollow" target="_blank" href="http://himmerweb.com/">Himmer Web Solutions</a>
                    </div>
                    <div class="span6">
                    	<nav>
                          <ul>
                            <li><a href="index.php">Home</a></li>
                            <li><a href="amenities.php">Amenities</a></li>
                            <li><a href="attractions.php">Attractions</a></li>
                            <li><a href="rooms-rates.php">Rooms & Rates</a></li>
                            <!-- <li><a href="#"></a></li> -->
                            <li><a href="contact.php">Contact</a></li>
                          </ul>
                        </nav>
                  </div>
                </div>
                <br>
                <div class="row">
                  <div class="span2">
                    <a href="http://realresultsonline.com" rel="nofollow" target="_blank"><img src="img/logo-sm.png"></a>
                  </div>
                  <div class="span8"></div>
                <!--   <div class="span2">
                    <a rel="nofollow" target="_blank" href="http://himmerweb.com/"><img src="img/himmerweb.png"></a>
                  </div> -->
                </div>
            </div>
        </div>
        
   	</footer>
    <script>
    if (window.location.origin == "http://yellowstoneinn.net") {
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-61010816-1', 'auto');
      ga('send', 'pageview');
    };
  </script>